#include "WakerSettings.h"

WakerSettings::WakerSettings() :
    _settings(QSettings::IniFormat, QSettings::UserScope, "Cw", "Waker")
{
}

void WakerSettings::setOpacity(int opacity)
{
    _settings.setValue("opacity", opacity);
}

int WakerSettings::opacity() const
{
    return _settings.value("opacity", 100).toInt();
}

void WakerSettings::applyTo(QMainWindow *mainWindow)
{
    mainWindow->setWindowOpacity(opacity()/100.0);
    Qt::WindowFlags flags = mainWindow->windowFlags();
    if(staysOnTop()) {
        flags |= Qt::WindowStaysOnTopHint;
    }
    else {
        flags &= ~Qt::WindowStaysOnTopHint;
    }
    mainWindow->setWindowFlags(flags);
    mainWindow->show();
}

void WakerSettings::setStaysOnTop(bool staysontop)
{
    _settings.setValue("staysontop", staysontop);
}

bool WakerSettings::staysOnTop() const
{
    return _settings.value("staysontop", false).toBool();
}
