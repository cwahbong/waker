#ifndef WAKERSETTINGS_H
#define WAKERSETTINGS_H

#include <QMainWindow>
#include <QSettings>

class WakerSettings
{
public:
    explicit WakerSettings();

    void applyTo(QMainWindow *mainWindow);
    void setOpacity(int);
    void setStaysOnTop(bool);

    int opacity() const;
    bool staysOnTop() const;

private:
    QSettings _settings;
};

#endif // WAKERSETTINGS_H
