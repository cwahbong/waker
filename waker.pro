#-------------------------------------------------
#
# Project created by QtCreator 2012-06-09T18:22:08
#
#-------------------------------------------------

QT       += core gui

TARGET = waker
TEMPLATE = app

SOURCES += main.cpp \
    MainWindow.cpp \
    SettingsDialog.cpp \
    WakerSettings.cpp

HEADERS += MainWindow.h \
    SettingsDialog.h \
    WakerSettings.h

FORMS += MainWindow.ui \
    SettingsDialog.ui

TRANSLATIONS += zh_TW.ts

RESOURCES += icons.qrc

OTHER_FILES += icons.rc

win32:RC_FILE = icons.rc
macx:LIBS += -framework CoreFoundation
