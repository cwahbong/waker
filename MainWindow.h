#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionAboutWaker_triggered();
    void on_actionAboutQt_triggered();
    void on_actionSettings_triggered();
    void on_pushButton_clicked();
    void on_nameListButton_clicked();

private:
    Ui::MainWindow *ui;
    QStringList _namelist;
};

#endif // MAINWINDOW_H
