#include "MainWindow.h"

#include <ctime>

#include <QLayout>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

#include "ui_MainWindow.h"
#include "SettingsDialog.h"
#include "WakerSettings.h"

using std::time;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    layout()->setSizeConstraint(QLayout::SetFixedSize);
    qsrand(time(NULL));
    WakerSettings().applyTo(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void
MainWindow::on_actionAboutWaker_triggered()
{
    const QString title = "About Waker";
    const QString content = "Waker is suitable for waking somebody up in your class.";
    QMessageBox::about(this, title, content);
}

void
MainWindow::on_actionAboutQt_triggered()
{
    QApplication::aboutQt();
}

void
MainWindow::on_actionSettings_triggered()
{
    SettingsDialog(this, this).exec();
}

void MainWindow::on_pushButton_clicked()
{
    if(_namelist.empty()) {
        QMessageBox::warning(this, "Not a valid name list.",
                "Please choose a non-empty namelist vefore waking people.");
    }
    else {
        const QString& picked = _namelist.at(qrand()%_namelist.length());
        ui->lineEdit->setText(picked);
    }
}

void MainWindow::on_nameListButton_clicked()
{
    const QString& filename = QFileDialog::getOpenFileName(this, "Open a name list");
    if(!filename.isNull()) {
        ui->nameListButton->setText(filename);
        _namelist.clear();
        QFile file(filename);
        if(file.open(QFile::ReadOnly)) {
            QTextStream stream(&file);
            while(!stream.atEnd()) {
                const QString& name = stream.readLine().trimmed();
                if(name.length()>0) {
                    if(name.length()<256) {
                        _namelist.append(name);
                    }
                    else {
                        // Name too long
                    }
                }
                if(_namelist.length()>1000) {
                    QMessageBox::warning(this, "Warning",
                            "Name list too long.  The list will be truncated.");
                    break;
                }
            }
        }
        else {
            QMessageBox::critical(this, "Error", "Cannot open file.");
        }
    }
}
