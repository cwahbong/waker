#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QMainWindow>

#include "WakerSettings.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SettingsDialog(QWidget *parent = 0, QMainWindow *mainWindow = 0);
    ~SettingsDialog();

public slots:
    virtual void accept();
    virtual void reject();

private slots:
    void on_opacitySlider_valueChanged(int value);

private:
    Ui::SettingsDialog *ui;
    QMainWindow* _mainWindow;
    WakerSettings _settings;
};

#endif // SETTINGS_H
