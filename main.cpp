#include <QtGui/QApplication>
#include "MainWindow.h"

#if defined(Q_OS_MAC)
#  include <CoreFoundation/CoreFoundation.h>
#  include <unistd.h>
#endif

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    
#if defined(Q_OS_MAC)
    CFBundleRef bundle = CFBundleGetMainBundle();
    CFURLRef url = CFBundleCopyBundleURL(bundle);
    CFStringRef path = CFURLCopyFileSystemPath(url, kCFURLPOSIXPathStyle);
    const char *p = CFStringGetCStringPtr(path, CFStringGetSystemEncoding());
    qDebug("Path = %s", p);
    
    chdir(p);
    chdir("..");
    
    CFRelease(path);
    CFRelease(url);
#endif
    
    MainWindow w;
    w.show();
    
    return app.exec();
}
