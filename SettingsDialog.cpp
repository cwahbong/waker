#include "SettingsDialog.h"
#include "ui_SettingsDialog.h"

SettingsDialog::SettingsDialog(QWidget *parent, QMainWindow *mainWindow) :
    QDialog(parent),
    ui(new Ui::SettingsDialog),
    _mainWindow(mainWindow)
{
    ui->setupUi(this);
    if(_mainWindow) {
        ui->opacitySlider->setValue(_settings.opacity());
        ui->onTopCheckBox->setChecked(_settings.staysOnTop());
    }
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::accept()
{
    _settings.setOpacity(ui->opacitySlider->value());
    _settings.setStaysOnTop(ui->onTopCheckBox->isChecked());
    _settings.applyTo(_mainWindow);
    QDialog::accept();
}

void SettingsDialog::reject()
{
    _settings.applyTo(_mainWindow);
    QDialog::reject();
}

void SettingsDialog::on_opacitySlider_valueChanged(int value)
{
    if(_mainWindow) {
        _mainWindow->setWindowOpacity(value / 100.0);
    }
}
