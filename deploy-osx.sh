#!/bin/bash

# Parameter 1: The path where the waker.app takes place
# Parameter 2: The path where the Qt Frameworks is

#
cd $1
cd waker.app/Contents

#
mkdir Frameworks

#
cp -R $2/QtCore.framework Frameworks/
cp -R $2/QtGui.framework Frameworks/

#
fwk_prefix=@executable_path/../Frameworks
qtcore_suffix=QtCore.framework/Versions/4/QtCore
qtgui_suffix=QtGui.framework/Versions/4/QtGui

#
old_qtcore=$2/$qtcore_suffix
new_qtcore=$fwk_prefix/$qtcore_suffix
old_qtgui=$2/$qtgui_suffix
new_qtgui=$fwk_prefix/$qtgui_suffix

#
#install_name_tool -id $new_qtcore Frameworks/$qtcore_suffix

#install_name_tool -id $new_qtgui Frameworks/$qtgui_suffix

#
install_name_tool -change $old_qtcore $new_qtcore MacOS/waker

install_name_tool -change $old_qtgui $new_qtgui MacOS/waker

#
install_name_tool -change $old_qtcore $new_qtcore Frameworks/$qtgui_suffix

# Show
otool -L MacOS/waker
otool -L Frameworks/QtCore.framework/Versions/Current/QtCore
otool -L Frameworks/QtGui.framework/Versions/Current/QtGui

